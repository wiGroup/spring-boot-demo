package com.wigroup.invoices.domain.invoice.entity;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.wigroup.invoices.domain.invoice.dto.InvoiceDto;
import com.wigroup.invoices.domain.lineitem.dto.LineItemDto;
import com.wigroup.invoices.domain.lineitem.entity.LineItem;

@Entity
@Table(name = "invoice")
public class Invoice {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "client", nullable = false)
	private String client;
	
	@Column(name = "vat_rate", nullable = false)
	private Long vatRate;
	
	@Column(name = "invoice_date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date invoiceDate;
	
	@OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL)
    private Set<LineItem> lineItems;

	public Invoice() {
	}

	public Invoice(String client, Long vatRate, Date invoiceDate, List<LineItemDto> lineItems) {
		this.client = client;
		this.vatRate = vatRate;
		this.invoiceDate = invoiceDate;
		this.lineItems = lineItems.stream().map(e -> new LineItem(e)).collect(Collectors.toSet());
	}
	
	public Invoice(InvoiceDto invoice, Set<LineItem> lineItems) {
		this.client = invoice.getClient();
		this.vatRate = invoice.getVatRate();
		this.invoiceDate = invoice.getInvoiceDate();
		this.lineItems = lineItems;
	}

	
	public Invoice(InvoiceDto invoice) {
		this.client = invoice.getClient();
		this.vatRate = invoice.getVatRate();
		this.invoiceDate = invoice.getInvoiceDate();
		this.lineItems = invoice.getLineItems().stream().map(e -> new LineItem(e)).collect(Collectors.toSet());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public Long getVatRate() {
		return vatRate;
	}

	public void setVatRate(Long vatRate) {
		this.vatRate = vatRate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Set<LineItem> getLineItems() {
		return lineItems;
	}

	public void setLineItems(Set<LineItem> lineItems) {
		this.lineItems = lineItems;
	}
}
