package com.wigroup.invoices.domain.invoice.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.wigroup.invoices.domain.invoice.dto.InvoiceDto;
import com.wigroup.invoices.domain.invoice.entity.Invoice;
import com.wigroup.invoices.domain.invoice.entity.InvoiceRepository;
import com.wigroup.invoices.domain.lineitem.entity.LineItem;
import com.wigroup.invoices.exceptions.ResourceNotFoundException;

@Service
public class InvoiceService implements IInvoiceService {
	
	@Autowired
	private InvoiceRepository invoiceRepository;
	
	@Autowired
	private MessageSource messageSource;
	
	public InvoiceDto viewInvoice(Long id) {

		Invoice invoice = invoiceRepository
					.findById(id)
					.orElseThrow(() -> 
						new ResourceNotFoundException(
							messageSource.getMessage("invoicerest.invoice.not.found", null, LocaleContextHolder.getLocale()) + id));
		
		InvoiceDto thisInvoice =  new InvoiceDto(invoice);
		
		return thisInvoice;
	}
	
	public List<InvoiceDto> viewAllInvoices() {
		
		List<Invoice> invoices = invoiceRepository.findAll();
		
		List<InvoiceDto> invoicesDtos = new ArrayList<>();
		
		invoices.parallelStream().forEach(e -> {
			invoicesDtos.add(new InvoiceDto(e));
		});
		
		return invoicesDtos;	
	}
	
	public InvoiceDto addInvoice(InvoiceDto invoice) {
		
		final Invoice thisInvoice = new Invoice(invoice);
		
		Set<LineItem> lineItems = new HashSet<>();
		invoice.getLineItems().forEach(e -> {
			LineItem thisLineItem = new LineItem(e, thisInvoice);
			lineItems.add(thisLineItem);
			thisInvoice.setLineItems(lineItems);	
		});
		invoiceRepository.save(thisInvoice);

		return new InvoiceDto(thisInvoice);
	}
}
