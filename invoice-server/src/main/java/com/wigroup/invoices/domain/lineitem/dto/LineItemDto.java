package com.wigroup.invoices.domain.lineitem.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.wigroup.invoices.domain.lineitem.entity.LineItem;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "All details about the LineItem. ")
public class LineItemDto {

	@ApiModelProperty(notes = "The database generated LineItemDto ID")
	private long id;
	
	@ApiModelProperty(notes = "The LineItemDto quantity")
	private Long quantity;

	@ApiModelProperty(notes = "The LineItemDto description")
	private String description;
	
	
	@ApiModelProperty(notes = "The LineItemDto unitPrice")
	private BigDecimal unitPrice;
	
	public LineItemDto(LineItem lineItem) {
		
		this.id = lineItem.getId();
		this.quantity = lineItem.getQuantity();
		this.description = lineItem.getDescription();
		this.unitPrice = lineItem.getUnitPrice();
	}
	
	protected LineItemDto(){}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public Long getQuantity() {
		return quantity;
	}


	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public BigDecimal getUnitPrice() {
		return unitPrice;
	}


	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public BigDecimal getLineItemTotal() {
		return this.unitPrice.multiply(new BigDecimal(this.quantity)).setScale(2, RoundingMode.HALF_UP);
	}
}
