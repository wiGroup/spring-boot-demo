package com.wigroup.invoices.domain.lineitem.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.wigroup.invoices.domain.invoice.entity.Invoice;
import com.wigroup.invoices.domain.lineitem.dto.LineItemDto;

@Entity
@Table(name = "lineitem")
public class LineItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "quantity", nullable = false)
	private Long quantity;
	
	@Column(name = "description", nullable = true)
	private String description;
	
	
	@Column(name = "unitPrice", nullable = false)
	private BigDecimal unitPrice;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "invoice_id")
    private Invoice invoice;

	protected LineItem() {
	}

	public LineItem(Long quantity, String description, BigDecimal unitPrice) {
		this.quantity = quantity;
		this.description = description;
		this.unitPrice = unitPrice;
	}
	
	public LineItem(LineItemDto lineItemDto) {
		this.description = lineItemDto.getDescription();
		this.quantity = lineItemDto.getQuantity();
		this.unitPrice = lineItemDto.getUnitPrice();
	}
	
	public LineItem(LineItemDto lineItemDto, Invoice invoice) {
		this.description = lineItemDto.getDescription();
		this.quantity = lineItemDto.getQuantity();
		this.unitPrice = lineItemDto.getUnitPrice();
		this.invoice = invoice;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	@Override
	public String toString() {
		return "LineItem [id=" + id + ", quantity=" + quantity + ", description=" + description + ", unitPrice="
				+ unitPrice + "]";
	}
}
