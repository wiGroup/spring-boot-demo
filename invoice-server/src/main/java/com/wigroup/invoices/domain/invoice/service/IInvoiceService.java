package com.wigroup.invoices.domain.invoice.service;

import java.util.List;

import com.wigroup.invoices.domain.invoice.dto.InvoiceDto;

public interface IInvoiceService {
	
	public InvoiceDto viewInvoice(Long id);
	
	public List<InvoiceDto> viewAllInvoices();
	
	public InvoiceDto addInvoice(InvoiceDto invoice);

}
