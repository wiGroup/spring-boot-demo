package com.wigroup.invoices.domain.invoice.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wigroup.invoices.domain.invoice.entity.Invoice;
import com.wigroup.invoices.domain.lineitem.dto.LineItemDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "All details about the InvoiceDto. ")
public class InvoiceDto {

	@ApiModelProperty(notes = "The database generated Invoice ID")
	private long id;
	
	@ApiModelProperty(notes = "The invoice client")
	@NotNull
	@Size(min = 1, max = 70)
	private String client;
	
	@ApiModelProperty(notes = "The vatRate")
	@NotNull
	@Range(min = 1, max = 20, message = "Vat rate must be between 1 and 20")
	private Long vatRate;
	
	@ApiModelProperty(notes = "The invoiceDate", example = "2019-10-22")
	private Date invoiceDate;
	
	private BigDecimal subTotal = new BigDecimal(0);
	
	private BigDecimal total;
	
	private List<LineItemDto> lineItems;

	public InvoiceDto(Invoice invoice) {
		this.id = invoice.getId();
		this.client = invoice.getClient();
		this.vatRate = invoice.getVatRate();
		this.invoiceDate = invoice.getInvoiceDate();
		this.lineItems = invoice.getLineItems().stream().map(e -> new LineItemDto(e)).collect(Collectors.toList());

		for (LineItemDto thisLineItem : lineItems) {
			this.subTotal = thisLineItem.getLineItemTotal().add(subTotal).setScale(2, RoundingMode.HALF_UP);
		}
		this.total = subTotal.multiply(new BigDecimal(this.vatRate)).setScale(2, RoundingMode.HALF_UP);
	}

	protected InvoiceDto() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	@JsonIgnore
	public BigDecimal getVat() {
		return new BigDecimal(this.vatRate).setScale(2, RoundingMode.HALF_UP);
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public List<LineItemDto> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<LineItemDto> lineItems) {
		this.lineItems = lineItems;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}
	
	public BigDecimal getTotal() {
		return this.total;
	}

	public Long getVatRate() {
		return vatRate;
	}
}
