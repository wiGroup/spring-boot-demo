package com.wigroup.invoices.app.rest;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.wigroup.invoices.domain.invoice.dto.InvoiceDto;
import com.wigroup.invoices.domain.invoice.service.IInvoiceService;
import com.wigroup.invoices.exceptions.ResourceNotFoundException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/invoices")
@Api(value = "Invoice Management System", description = "Operations pertaining to invoice in Invoice Management System")
public class InvoiceRest {

	@Autowired
	private IInvoiceService invoiceService;

	@ApiOperation(value = "View a list of available invoices", response = List.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successfully retrieved list", response = List.class),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping()
	public List<InvoiceDto> viewAllInvoices() {
		
		return invoiceService.viewAllInvoices();
	}

	@ApiOperation(consumes = "application/json, application/xml", value = "Get an invoice by Id")
	@GetMapping("/{id}")
	public ResponseEntity<InvoiceDto> viewInvoice(
			@ApiParam(value = "The id of the invoice", required = true) @PathVariable(value = "id") Long invoiceId)
			throws ResourceNotFoundException {
		
		InvoiceDto invoice = invoiceService.viewInvoice(invoiceId);
		
		return ResponseEntity.ok(invoice);
	}

	@ApiOperation(value = "Add an Invoice")
	@PostMapping()
	public ResponseEntity<InvoiceDto> addInvoice(
			@ApiParam(value = "InvoiceDto request object to save", required = true) @Valid @RequestBody InvoiceDto invoiceDto) {
		
		InvoiceDto thisInvoice = invoiceService.addInvoice(invoiceDto);

		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(thisInvoice.getId())
				.toUri();
		
		return ResponseEntity.created(location).body(thisInvoice);
	}
}
