package com.wigroup.domain.orders.dto;

public class UserOrdersDto {
	
	private String orderName;
	
	private UserOrdersDto() {}
	
	public UserOrdersDto(String orderName) {
		this.orderName = orderName;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}
}
