package com.wigroup.domain.employee.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.wigroup.domain.employee.entity.Employee;
import com.wigroup.domain.orders.dto.UserOrdersDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "All details about the Employee. ")
public class EmployeeDto {

	@ApiModelProperty(notes = "The database generated employee ID")
	private long id;
	
	@ApiModelProperty(notes = "The employee first name")
	@NotNull
	@Size(min = 2, max = 70)
	private String firstName;
	
	@ApiModelProperty(notes = "The employee last name")
	@NotNull
	@Size(min = 2, max = 70)
	private String lastName;
	
	@ApiModelProperty(notes = "The employee email address")
	@Email
	private String emailAddress;

	private List<UserOrdersDto> orders;

	public EmployeeDto(Employee employee) {
		this.id = employee.getId();
		this.firstName = employee.getFirstName();
		this.lastName = employee.getLastName();
		this.emailAddress = employee.getEmailId();
		this.orders = new ArrayList<>();
	}

	public EmployeeDto() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public List<UserOrdersDto> getOrders() {
		return orders;
	}

	public void setOrders(List<UserOrdersDto> orders) {
		this.orders = orders;
	}

	@Override
	public String toString() {
		return "EmployeeDto [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", emailAddress="
				+ emailAddress + ", orders=" + orders + "]";
	}
}
