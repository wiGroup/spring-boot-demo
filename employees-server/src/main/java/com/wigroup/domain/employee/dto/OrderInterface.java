package com.wigroup.domain.employee.dto;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.wigroup.domain.orders.dto.UserOrdersDto;

@Service
public class OrderInterface {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@HystrixCommand(fallbackMethod = "getFallbackOrders")
	public List<UserOrdersDto> getEmployeeOrdersFromCVS(Long id) {
		UserOrdersDto orders = new UserOrdersDto("fries");
	
//		UserOrdersDto userOrders = restTemplate.getForObject("http://orders-app-server/api/vi/orders/" + employee.getId(), UserOrdersDto.class);
		return Arrays.asList(new UserOrdersDto("cheese burger"), new UserOrdersDto("fries"));
	}
	
	public List<UserOrdersDto> getFallbackOrders(Long id) {
		return Arrays.asList(new UserOrdersDto("cheese burger"), new UserOrdersDto("fries"));
	}

}
