package com.wigroup.domain.employee.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.wigroup.app.exceptions.ResourceNotFoundException;
import com.wigroup.domain.employee.dto.EmployeeDto;
import com.wigroup.domain.employee.dto.OrderInterface;
import com.wigroup.domain.employee.entity.Employee;
import com.wigroup.domain.employee.entity.EmployeeRepository;

@Service
@RefreshScope
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private OrderInterface orderInterface;
	
	public EmployeeDto getEmployeeById(Long id) {

		Employee employee = employeeRepository
					.findById(id)
					.orElseThrow(() -> 
						new ResourceNotFoundException(
							messageSource.getMessage("employeerest.employee.not.found", null, LocaleContextHolder.getLocale()) + id));
		
		EmployeeDto thisEmployee =  new EmployeeDto(employee);
		
		thisEmployee.setOrders(orderInterface.getEmployeeOrdersFromCVS(id));
		
		return thisEmployee;
	}
	
	public List<EmployeeDto> getAllEmployees() {
		
		List<Employee> employees = employeeRepository.findAll();
		
		List<EmployeeDto> empDtos = new ArrayList<>();
		
		employees.parallelStream().forEach(e -> {
			empDtos.add(new EmployeeDto(e));
		});
		
		return empDtos;	
	}
	
	public EmployeeDto saveEmployee(EmployeeDto employee) {
		
		return new EmployeeDto(employeeRepository.save(new Employee(employee)));
	}
	
	public EmployeeDto updateEmployee(EmployeeDto employeeDto) {
		
		Employee employee = employeeRepository.findById(employeeDto.getId()).orElseThrow(() -> new ResourceNotFoundException(
					messageSource.getMessage("employeerest.employee.not.found", null, LocaleContextHolder.getLocale()) + employeeDto.getId()));

		employee.setEmailId(employeeDto.getEmailAddress());
		employee.setLastName(employeeDto.getLastName());
		employee.setFirstName(employeeDto.getFirstName());
		
		return new EmployeeDto(employeeRepository.save(employee));
	}
	
	public void deleteEmployee(Long id) {
		
		Employee employee = employeeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(
				messageSource.getMessage("employeerest.employee.not.found", null, LocaleContextHolder.getLocale())
						+ id));
		
		employeeRepository.delete(employee);
	}

}
