package com.wigroup.domain.login.dto;

public class TokenLoginResponseDto {

	private String jwttoken;

	public TokenLoginResponseDto(String jwttoken) {
		super();
		this.jwttoken = jwttoken;
	}

	public String getJwttoken() {
		return jwttoken;
	}

	public void setJwttoken(String jwttoken) {
		this.jwttoken = jwttoken;
	}
}
