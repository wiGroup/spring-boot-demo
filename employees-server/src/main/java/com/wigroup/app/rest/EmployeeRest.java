package com.wigroup.app.rest;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.wigroup.app.exceptions.ResourceNotFoundException;
import com.wigroup.domain.employee.dto.EmployeeDto;
import com.wigroup.domain.employee.service.EmployeeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RefreshScope
@RestController
@RequestMapping("rest/v1/employees")
@Api(value = "Employee Management System", description = "Operations pertaining to employee in Employee Management System")
public class EmployeeRest {
	
	@Autowired
	private EmployeeService employeeService;
	
	@Value("${config.from.repo}")
	private boolean fromRepo;

	@ApiOperation(value = "View a list of available employees", response = List.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successfully retrieved list", response = List.class),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping()
	@HystrixCommand
	public List<EmployeeDto> getAllEmployees() {
		
		List<EmployeeDto> employees = employeeService.getAllEmployees();
		log.info("getAllEmployees() response: {}", employees);
		log.info("getAllEmployees() response: {}", fromRepo);
		
		return employees;
	}

	@ApiOperation(consumes = "application/json, application/xml", value = "Get an employee by Id")
	@GetMapping("/{id}")
	@HystrixCommand
	public ResponseEntity<EmployeeDto> getEmployeeById(
			@ApiParam(value = "Employee id from which employee object will retrieve", required = true) @PathVariable(value = "id") Long employeeId)
			throws ResourceNotFoundException {
		
		EmployeeDto employee = employeeService.getEmployeeById(employeeId);
		log.info("getAllEmployees() response: {}", employee);
		
		return ResponseEntity.ok(employee);
	}

	@ApiOperation(value = "Add an employee")
	@PostMapping()
	public ResponseEntity<EmployeeDto> createEmployee(
			@ApiParam(value = "Employee object store in database table", required = true) @Valid @RequestBody EmployeeDto employee) {
		
		log.info("createEmployee @RequestBody: {}", employee.toString());
		
		EmployeeDto thisEmployee = employeeService.saveEmployee(employee);

		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(thisEmployee.getId())
				.toUri();
		
		return ResponseEntity.created(location).body(thisEmployee);
	}

	@ApiOperation(value = "Update an employee")
	@PutMapping("/{id}")
	public ResponseEntity<EmployeeDto> updateEmployee(
			@ApiParam(value = "Employee Id to update employee object", required = true) @PathVariable(value = "id") Long employeeId,
			@ApiParam(value = "Update employee object", required = true) @Valid @RequestBody EmployeeDto employeeDetails)
			throws ResourceNotFoundException {
		
		employeeDetails.setId(employeeId);
		
		final EmployeeDto updatedEmployee = employeeService.updateEmployee(employeeDetails);
		
		return ResponseEntity.ok(updatedEmployee);
	}

	@ApiOperation(value = "Delete an employee")
	@DeleteMapping("/{id}")
	public Map<String, Boolean> deleteEmployee(
			@ApiParam(value = "Employee Id from which employee object will delete from database table", required = true) @PathVariable(value = "id") Long employeeId)
			throws ResourceNotFoundException {
		
		employeeService.deleteEmployee(employeeId);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		
		return response;
	}
}
