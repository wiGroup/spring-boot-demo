package com.wigroup.app.rest;
//package com.wigroup.spot.app.rest;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.DisabledException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.wigroup.spot.app.util.JwtTokenUtil;
//import com.wigroup.spot.domain.login.dto.LoginRequestDto;
//import com.wigroup.spot.domain.login.dto.TokenLoginResponseDto;
//
//@RestController
//@CrossOrigin
//@RequestMapping("/api/v1/login")
//public class AuthenticationRest {
//
//	@Autowired
//	private AuthenticationManager authenticationManager;
//
////	@Autowired
////	private JwtUserDetailsService userDetailsService;
//
//	@Autowired
//	private JwtTokenUtil jwtTokenUtil;
//
//	@PostMapping(value = "")
//	public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginRequestDto authenticationRequest)
//			throws Exception {
//		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
//		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
//		final String token = jwtTokenUtil.generateToken(userDetails);
//		return ResponseEntity.ok(new TokenLoginResponseDto(token));
//	}
//
//	private void authenticate(String username, String password) throws Exception {
//		try {
//			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
//		} catch (DisabledException e) {
//			throw new Exception("USER_DISABLED", e);
//		} catch (BadCredentialsException e) {
//			throw new Exception("INVALID_CREDENTIALS", e);
//		}
//	}
//}
