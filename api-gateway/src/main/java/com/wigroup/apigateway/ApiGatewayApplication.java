package com.wigroup.apigateway;

import java.util.UUID;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.gateway.filter.ratelimit.RedisRateLimiter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import brave.sampler.Sampler;

@SpringBootApplication
@EnableEurekaClient
public class ApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}
	
	@Bean
	public RouteLocator myRoutes(RouteLocatorBuilder builder) {
		
		String correlationId = UUID.randomUUID().toString();
		
	    return builder.routes()
	        .route("invoices-server", p -> p.path("/api/invoices/**")
	        	.filters(f -> f.stripPrefix(1)
	            .addRequestHeader("gateway-request-id", correlationId)
	            .addResponseHeader("gateway-response-id", correlationId))
	            .uri("lb://INVOICES-SERVER").id("invoices-server"))
//	        	.uri("http://localhost:8085"))
	        
	        .route("employees-server", p -> p.path("/api/employees/**")
	        	.filters(f -> f.stripPrefix(1).prefixPath("/rest/v1")
	        	.addRequestHeader("gateway-request-id", correlationId)
	        	.addResponseHeader("gateway-response-id", correlationId))
	            .uri("lb://EMPLOYEES-SERVER").id("employees-server"))
//	        	.uri("http://localhost:8083"))
	        .build();
	}
	
	@Bean
	RedisRateLimiter redisRateLimiter() {
		return new RedisRateLimiter(10, 20);
	}
	
	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}

}
