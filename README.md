

### What is this repository for? ###

* Overview of the spring boot microservice system, putting everything together.

### How do I get set up? ###

* setup a bitbucket repo to store your configuration files, copy files from folder: cloud-config-repo
* provide login credentials to your configuration repo, specify them in the application.yml username and password field


### Contribution guidelines ###

* locate registered services at: http://localhost:8761/eureka
* locate configs at: http://localhost:8888/employees-server/dev
* test api-gateway at: http://localhost:8080/api/employees
